using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CafeApps.Win10.Views.Destinasi
{
    /// <summary>
    /// Interaction logic for Destinasi.xaml
    /// </summary>
    public partial class Destinasi : UserControl
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-F7PGM75;Initial Catalog=wisata_one;Integrated Security=True");
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        public Destinasi()
        {
            InitializeComponent();
            SqlCommand cmd = new SqlCommand("Select * from Objek_Wisata", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmd.Dispose();
            da.Dispose();
            con.Close();
            ViewOW.ItemsSource = dt.DefaultView;
        }
        
        
        private void BtnView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataRowView dataRowView = (DataRowView)((Button)e.Source).DataContext;
                String Kode = dataRowView[0].ToString();
                String Nama = dataRowView[1].ToString();
                String Lokasi = dataRowView[2].ToString();
                String Deskrip = dataRowView[3].ToString();
                String Waktu = dataRowView[4].ToString();
                MessageBox.Show("Kode : " + Kode + 
                                "\r\n\nTempat Wisata : " + Nama + 
                                "\r\n\nLokasi : " + Lokasi +
                                "\r\n\nDeskripsi : " + Deskrip +
                                "\r\n\nWaktu Operasional : " + Waktu, "Detail Obejek Wisata" );
                //This is the code which will show the button click row data. Thank you.
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DestinasiForms F = new DestinasiForms();
            F.Show();
        }

    }
}
