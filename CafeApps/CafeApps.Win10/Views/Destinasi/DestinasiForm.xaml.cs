using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CafeApps.Win10.ViewModels;
using System.Windows.Forms;
using CafeApps.Win10.Models;
using System.Collections.ObjectModel;

namespace CafeApps.Win10.Views.Destinasi
{
    /// <summary>
    /// Interaction logic for DestinasiForm.xaml
    /// </summary>
    public partial class DestinasiForm : Window
    {
        private DestinasiViewModel vm;
        public DestinasiForm()
        {
            InitializeComponent();

            vm = new DestinasiViewModel();
            //vm.DestinasiBS = load_SourceUpdated;
            this.Loaded += delegate { vm.Load(); };
            btnNew.Click += delegate { vm.New(); };
            btnSave.Click += delegate { vm.Save(); };
            btnDelete.Click += delegate { vm.Delete(); };
            this.Closing += delegate { vm.Dispose(); };
        }

        private void load_SourceUpdated(object sender, DataTransferEventArgs e)
        {

        }
    }
}
