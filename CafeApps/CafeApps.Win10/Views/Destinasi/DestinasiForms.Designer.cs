
namespace CafeApps.Win10.Views.Destinasi
{
    partial class DestinasiForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label deskripsiLabel;
            System.Windows.Forms.Label kodeOWLabel;
            System.Windows.Forms.Label lokasiLabel;
            System.Windows.Forms.Label namaLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DestinasiForms));
            System.Windows.Forms.Label waktu_OperasionalLabel;
            this.objek_WisataBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.objek_WisataBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.deskripsiTextBox = new System.Windows.Forms.TextBox();
            this.kodeOWTextBox = new System.Windows.Forms.TextBox();
            this.lokasiTextBox = new System.Windows.Forms.TextBox();
            this.namaTextBox = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.kodeOWDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lokasiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deskripsiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waktuOperasionalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.objek_WisataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.waktu_OperasionalTextBox = new System.Windows.Forms.TextBox();
            deskripsiLabel = new System.Windows.Forms.Label();
            kodeOWLabel = new System.Windows.Forms.Label();
            lokasiLabel = new System.Windows.Forms.Label();
            namaLabel = new System.Windows.Forms.Label();
            waktu_OperasionalLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.objek_WisataBindingNavigator)).BeginInit();
            this.objek_WisataBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objek_WisataBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // deskripsiLabel
            // 
            deskripsiLabel.AutoSize = true;
            deskripsiLabel.Location = new System.Drawing.Point(58, 114);
            deskripsiLabel.Name = "deskripsiLabel";
            deskripsiLabel.Size = new System.Drawing.Size(53, 13);
            deskripsiLabel.TabIndex = 1;
            deskripsiLabel.Text = "Deskripsi:";
            // 
            // kodeOWLabel
            // 
            kodeOWLabel.AutoSize = true;
            kodeOWLabel.Location = new System.Drawing.Point(12, 34);
            kodeOWLabel.Name = "kodeOWLabel";
            kodeOWLabel.Size = new System.Drawing.Size(57, 13);
            kodeOWLabel.TabIndex = 3;
            kodeOWLabel.Text = "Kode OW:";
            // 
            // lokasiLabel
            // 
            lokasiLabel.AutoSize = true;
            lokasiLabel.Location = new System.Drawing.Point(70, 87);
            lokasiLabel.Name = "lokasiLabel";
            lokasiLabel.Size = new System.Drawing.Size(41, 13);
            lokasiLabel.TabIndex = 5;
            lokasiLabel.Text = "Lokasi:";
            // 
            // namaLabel
            // 
            namaLabel.AutoSize = true;
            namaLabel.Location = new System.Drawing.Point(73, 61);
            namaLabel.Name = "namaLabel";
            namaLabel.Size = new System.Drawing.Size(38, 13);
            namaLabel.TabIndex = 7;
            namaLabel.Text = "Nama:";
            // 
            // objek_WisataBindingNavigator
            // 
            this.objek_WisataBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.objek_WisataBindingNavigator.BindingSource = this.objek_WisataBindingSource;
            this.objek_WisataBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.objek_WisataBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.objek_WisataBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.objek_WisataBindingNavigatorSaveItem});
            this.objek_WisataBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.objek_WisataBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.objek_WisataBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.objek_WisataBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.objek_WisataBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.objek_WisataBindingNavigator.Name = "objek_WisataBindingNavigator";
            this.objek_WisataBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.objek_WisataBindingNavigator.Size = new System.Drawing.Size(808, 25);
            this.objek_WisataBindingNavigator.TabIndex = 0;
            this.objek_WisataBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // objek_WisataBindingNavigatorSaveItem
            // 
            this.objek_WisataBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.objek_WisataBindingNavigatorSaveItem.Enabled = false;
            this.objek_WisataBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("objek_WisataBindingNavigatorSaveItem.Image")));
            this.objek_WisataBindingNavigatorSaveItem.Name = "objek_WisataBindingNavigatorSaveItem";
            this.objek_WisataBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.objek_WisataBindingNavigatorSaveItem.Text = "Save Data";
            // 
            // deskripsiTextBox
            // 
            this.deskripsiTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.objek_WisataBindingSource, "Deskripsi", true));
            this.deskripsiTextBox.Location = new System.Drawing.Point(117, 111);
            this.deskripsiTextBox.Name = "deskripsiTextBox";
            this.deskripsiTextBox.Size = new System.Drawing.Size(610, 20);
            this.deskripsiTextBox.TabIndex = 2;
            // 
            // kodeOWTextBox
            // 
            this.kodeOWTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.objek_WisataBindingSource, "KodeOW", true));
            this.kodeOWTextBox.Location = new System.Drawing.Point(75, 31);
            this.kodeOWTextBox.Name = "kodeOWTextBox";
            this.kodeOWTextBox.Size = new System.Drawing.Size(68, 20);
            this.kodeOWTextBox.TabIndex = 4;
            // 
            // lokasiTextBox
            // 
            this.lokasiTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.objek_WisataBindingSource, "Lokasi", true));
            this.lokasiTextBox.Location = new System.Drawing.Point(117, 84);
            this.lokasiTextBox.Name = "lokasiTextBox";
            this.lokasiTextBox.Size = new System.Drawing.Size(610, 20);
            this.lokasiTextBox.TabIndex = 6;
            // 
            // namaTextBox
            // 
            this.namaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.objek_WisataBindingSource, "Nama", true));
            this.namaTextBox.Location = new System.Drawing.Point(117, 58);
            this.namaTextBox.Name = "namaTextBox";
            this.namaTextBox.Size = new System.Drawing.Size(205, 20);
            this.namaTextBox.TabIndex = 8;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kodeOWDataGridViewTextBoxColumn,
            this.namaDataGridViewTextBoxColumn,
            this.lokasiDataGridViewTextBoxColumn,
            this.deskripsiDataGridViewTextBoxColumn,
            this.waktuOperasionalDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.objek_WisataBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 175);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(776, 172);
            this.dataGridView1.TabIndex = 11;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(713, 353);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(632, 353);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(551, 353);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 14;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            // 
            // kodeOWDataGridViewTextBoxColumn
            // 
            this.kodeOWDataGridViewTextBoxColumn.DataPropertyName = "KodeOW";
            this.kodeOWDataGridViewTextBoxColumn.HeaderText = "KodeOW";
            this.kodeOWDataGridViewTextBoxColumn.Name = "kodeOWDataGridViewTextBoxColumn";
            this.kodeOWDataGridViewTextBoxColumn.Width = 70;
            // 
            // namaDataGridViewTextBoxColumn
            // 
            this.namaDataGridViewTextBoxColumn.DataPropertyName = "Nama";
            this.namaDataGridViewTextBoxColumn.HeaderText = "Nama";
            this.namaDataGridViewTextBoxColumn.Name = "namaDataGridViewTextBoxColumn";
            this.namaDataGridViewTextBoxColumn.Width = 150;
            // 
            // lokasiDataGridViewTextBoxColumn
            // 
            this.lokasiDataGridViewTextBoxColumn.DataPropertyName = "Lokasi";
            this.lokasiDataGridViewTextBoxColumn.HeaderText = "Lokasi";
            this.lokasiDataGridViewTextBoxColumn.Name = "lokasiDataGridViewTextBoxColumn";
            this.lokasiDataGridViewTextBoxColumn.Width = 200;
            // 
            // deskripsiDataGridViewTextBoxColumn
            // 
            this.deskripsiDataGridViewTextBoxColumn.DataPropertyName = "Deskripsi";
            this.deskripsiDataGridViewTextBoxColumn.HeaderText = "Deskripsi";
            this.deskripsiDataGridViewTextBoxColumn.Name = "deskripsiDataGridViewTextBoxColumn";
            this.deskripsiDataGridViewTextBoxColumn.Width = 210;
            // 
            // waktuOperasionalDataGridViewTextBoxColumn
            // 
            this.waktuOperasionalDataGridViewTextBoxColumn.DataPropertyName = "Waktu_Operasional";
            this.waktuOperasionalDataGridViewTextBoxColumn.HeaderText = "Waktu_Operasional";
            this.waktuOperasionalDataGridViewTextBoxColumn.Name = "waktuOperasionalDataGridViewTextBoxColumn";
            // 
            // objek_WisataBindingSource
            // 
            this.objek_WisataBindingSource.DataSource = typeof(CafeApps.Win10.Models.Objek_Wisata);
            // 
            // waktu_OperasionalLabel
            // 
            waktu_OperasionalLabel.AutoSize = true;
            waktu_OperasionalLabel.Location = new System.Drawing.Point(10, 144);
            waktu_OperasionalLabel.Name = "waktu_OperasionalLabel";
            waktu_OperasionalLabel.Size = new System.Drawing.Size(101, 13);
            waktu_OperasionalLabel.TabIndex = 14;
            waktu_OperasionalLabel.Text = "Waktu Operasional:";
            // 
            // waktu_OperasionalTextBox
            // 
            this.waktu_OperasionalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.objek_WisataBindingSource, "Waktu_Operasional", true));
            this.waktu_OperasionalTextBox.Location = new System.Drawing.Point(117, 141);
            this.waktu_OperasionalTextBox.Name = "waktu_OperasionalTextBox";
            this.waktu_OperasionalTextBox.Size = new System.Drawing.Size(100, 20);
            this.waktu_OperasionalTextBox.TabIndex = 15;
            // 
            // DestinasiForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(808, 396);
            this.Controls.Add(waktu_OperasionalLabel);
            this.Controls.Add(this.waktu_OperasionalTextBox);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(namaLabel);
            this.Controls.Add(this.namaTextBox);
            this.Controls.Add(lokasiLabel);
            this.Controls.Add(this.lokasiTextBox);
            this.Controls.Add(kodeOWLabel);
            this.Controls.Add(this.kodeOWTextBox);
            this.Controls.Add(deskripsiLabel);
            this.Controls.Add(this.deskripsiTextBox);
            this.Controls.Add(this.objek_WisataBindingNavigator);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Name = "DestinasiForms";
            this.Text = "DestinasiForms";
            this.Load += new System.EventHandler(this.DestinasiForms_Load);
            ((System.ComponentModel.ISupportInitialize)(this.objek_WisataBindingNavigator)).EndInit();
            this.objek_WisataBindingNavigator.ResumeLayout(false);
            this.objek_WisataBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objek_WisataBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource objek_WisataBindingSource;
        private System.Windows.Forms.BindingNavigator objek_WisataBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton objek_WisataBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox deskripsiTextBox;
        private System.Windows.Forms.TextBox kodeOWTextBox;
        private System.Windows.Forms.TextBox lokasiTextBox;
        private System.Windows.Forms.TextBox namaTextBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kodeOWDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lokasiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deskripsiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn waktuOperasionalDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.TextBox waktu_OperasionalTextBox;
    }
}