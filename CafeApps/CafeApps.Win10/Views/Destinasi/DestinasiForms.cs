using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CafeApps.Win10.ViewModels;

namespace CafeApps.Win10.Views.Destinasi
{
    public partial class DestinasiForms : Form
    {
        private DestinasiViewModel vm;
        public DestinasiForms()
        {
            InitializeComponent();
            vm = new DestinasiViewModel();
            vm.DestinasiBS = objek_WisataBindingSource;
            this.Load += delegate { vm.Load(); };
            btnNew.Click += delegate { vm.New(); };
            btnSave.Click += delegate { vm.Save(); };
            btnDelete.Click += delegate { vm.Delete(); };
            this.Closing += delegate { vm.Dispose(); };
        }

        private void DestinasiForms_Load(object sender, EventArgs e)
        {

        }
    }
}
