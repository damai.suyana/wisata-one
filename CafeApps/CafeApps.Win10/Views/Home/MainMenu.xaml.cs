using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CafeApps.Win10.Views.Home
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            App.Viewrouting(false);
        }

        private void Destinasi_Click(object sender, RoutedEventArgs e)
        {
            App.Viewrouting(false);
            App.Viewrouting(true, new Destinasi.Destinasi());
        }

        private void Karyawan_Click(object sender, RoutedEventArgs e)
        {
            App.Viewrouting(false);
            App.Viewrouting(true, new Karyawan.Karyawan());
        }

        private void keluar_Click(object sender, RoutedEventArgs e)
        {
            MainMenu Tombolkeluar = Application.Current.MainWindow as MainMenu;
            Tombolkeluar.Close();
        }
    }
}
