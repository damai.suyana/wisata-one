using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using CafeApps.Win10.ViewModels;

namespace CafeApps.Win10.Views.Karyawan
{
    /// <summary>
    /// Interaction logic for InfoKaryawan.xaml
    /// </summary>
    public partial class Karyawan : UserControl
    {
        public Karyawan()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-F7PGM75;Initial Catalog=wisata_one;Integrated Security=True");
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        private void show()
        {
            da = new SqlDataAdapter("SELECT NIP, Nama, Profesi FROM Karyawan", con);
            dt = new DataTable();
            da.Fill(dt);
            ViewK.ItemsSource = dt.DefaultView;
            //KodeOW.Focus();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            show();
        }

        private void ViewK_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            KaryawanForms KF = new KaryawanForms();
            KF.Show();
        }
    }
}
