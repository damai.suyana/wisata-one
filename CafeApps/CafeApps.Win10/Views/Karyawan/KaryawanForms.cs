using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CafeApps.Win10.ViewModels;

namespace CafeApps.Win10.Views.Karyawan
{
    public partial class KaryawanForms : Form
    {
        private KaryawanViewModel vm;
        public KaryawanForms()
        {
            InitializeComponent();
            vm = new KaryawanViewModel();
            vm.KaryawanBS = karyawanBindingSource;
            this.Load += delegate { vm.Load(); };
            btnNew.Click += delegate { vm.New(); };
            btnSave.Click += delegate { vm.Save(); };
            btnDelete.Click += delegate { vm.Delete(); };
            this.Closing += delegate { vm.Dispose(); };
        }
    }
}
