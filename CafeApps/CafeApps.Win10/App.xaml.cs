using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using CafeApps.Win10.Views.Home;

namespace CafeApps.Win10
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static MainMenu Menu { get; set; }
        public static void Viewrouting(bool flag, Control content = null)
        {
            if (flag == true)
            {
                Menu.IsiMenu.Children.Add(content);
            }
            else
            {
                Menu.IsiMenu.Children.Clear();
            }
        }
        public static void Viewsetdrouting(bool flag1, Control control = null)
        {
            if(flag1 == true)
            {
                Menu.IsiMenu.Children.Add(control);
            }
            else 
            {
                Menu.IsiMenu.Children.Clear();
            }
        }
        public static void Viewsetkrouting(bool flag1, Control control1 = null)
        {
            if (flag1 == true)
            {
                Menu.IsiMenu.Children.Add(control1);
            }
            else
            {
                Menu.IsiMenu.Children.Clear();
            }
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Menu = new MainMenu();
            Menu.Show();
        }
    }
}
