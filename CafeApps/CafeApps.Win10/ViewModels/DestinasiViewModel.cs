using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CafeApps.Win10.Models;
using System.Windows.Forms;
using System.Data.Entity;


namespace CafeApps.Win10.ViewModels
{
    public class DestinasiViewModel : IDisposable
    {
        private OW db;
        public DestinasiViewModel() => db = new OW();
        public BindingSource DestinasiBS { get; set; }
        public void Load()
        {
            db.Objek_Wisata.Load();
            DestinasiBS.DataSource = db.Objek_Wisata.Local.ToBindingList();
        }
        public void Delete() => DestinasiBS.RemoveCurrent();
        public void New() => DestinasiBS.AddNew();
        public void Save()
        {
            DestinasiBS.EndEdit();
            db.SaveChanges();
        }
        public void Dispose()
        {
            db.Dispose();
        }
    }
}
