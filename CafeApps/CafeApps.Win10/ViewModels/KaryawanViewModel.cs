using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CafeApps.Win10.Models;
using System.Windows.Forms;
using System.Data.Entity;

namespace CafeApps.Win10.ViewModels
{
    class KaryawanViewModel : IDisposable
    {
        private K db;
        public KaryawanViewModel() => db = new K();
        public BindingSource KaryawanBS { get; set; }
        public void Load()
        {
            db.Karyawans.Load();
            KaryawanBS.DataSource = db.Karyawans.Local.ToBindingList();
        }
        public void Delete() => KaryawanBS.RemoveCurrent();
        public void New() => KaryawanBS.AddNew();
        public void Save()
        {
            KaryawanBS.EndEdit();
            db.SaveChanges();
        }
        public void Dispose()
        {
            db.Dispose();
        }
    }
}
